<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class HerokuPostDeploy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'postdeploy:heroku
                                   {--refresh : refresh database migrations.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run post-deploy on heroku';


    /**
     * HerokuPostDeploy constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
         * In production we only run migration
         */
        if (app()->environment('production')) {
            $this->call('migrate', ['--force',  true]);
            return;
        }
        /*
         * Refresh migrations
         */
        if ($this->option('refresh')) {
            $this->call('migrate:refresh');
        }
        $this->call('migrate');
    }
}
